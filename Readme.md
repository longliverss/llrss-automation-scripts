# ES

Estos son los scripts usados para automatizar la publicación de contenidos en llrss. 

## Prerequisitos

- imagemagick
- pngquant
- jpegoptim
- git
- md5sum

## 00_config.sh

Este archivo contiene las variables usadas en los otros scripts. Modifica lo que necesites. Recuerda:

- __las rutas deben ser absolutas para `$baseDir` y `$siteFolder`__

## 01_controlScript.sh

Este script es ejecutado por incron cuando detecta nuevos archivos en un directorio determinado (en este caso, manejado por Synthing). Primero, revisa que no haya 03_gitScripts en proceso. Después, crea una copia de las imágenes en el directorio del proyecto de Jekyll (creando un subdirectorio con la fecha en la que están siendo añadidas), las procesa (corta, comprime y renombra indicando prefijo, fecha y md5sum) y finalmente ejecuta `02_sleeplock.sh`. Recuerda:

- __modifica `source` en la línea 4 con la ruta absoluta de `00_config.sh`__

## 02_sleeplock.sh

Este es ejecutado por el script anterior. Sólo debe haber un proceso de `02_sleeplock.sh` ejecutándose (el resto son abortados al encontrar un `lockfile02` creado por la primera instancia ejecutada de `02_sleeplock.sh`). Este script se encarga de monitorear el folder de los `lockfiles01` (los creados por `01_controlScript.sh` con cada imagen copiada), pues si los hay, las imágenes aún están siendo procesadas. Cuando no haya más  `lockfiles01`, ejecuta `03_gitScript.sh`. Recuerda:

- __modifica `source` en la línea 4 con la ruta absoluta de `00_config.sh`__

## 03_gitScript.sh

Este es ejecutado por `02_sleeplock.sh`. Se asegura de que no haya ningún `03_gitScript.sh` en proceso y a continuación procede a añadir los cambios a git, hacer un commit y push. Recuerda:

- __modifica `source` en la línea 4 con la ruta absoluta de `00_config.sh`__

___

# EN

These are the scripts used to automate posting on llrss. 

You need to have installed:

- imagemagick
- pngquant
- jpegoptim
- git
- md5sum

# 00_config.sh

This file contains the variables used in subsequent scripts. Modify as needed. Remember:

- __paths must be absolute for `$baseDir` and `$siteFolder`__

# 01_controlScript.sh

This is launched by incron when detecting new files in a given folder (in this case, managed by Syncthing). First, it checks if there are no 03_gitScripts in process. Then, it creates a copy of the images in a target folder (where it creates a subfolder with the date the images were added), processes them (choping, compressing, renaming them) and finally launches `02_sleeplock.sh`. Remember to:

- __modify `source` in line 4 with the absolute path of `00_config.sh`__

# 02_sleeplock.sh

This is launched by the previous script. There must be only one `02_sleeplock.sh` process running (the rest are shut off when finding a `lockfile02` created by the first instance of `02_sleeplock.sh` launched). This script checks if there are `lockfiles01` (the ones created by `01_controlScript.sh`) in place indicating there are files being processed. If so, waits and checks again. If not, launches `03_gitScript.sh`. Remember to:

- __modify `source` in line 4 with the absolute path of `00_config.sh`__

# 03_gitScript.sh

This is launched by `02_sleeplock.sh`. It makes sure there are no `03_gitScript.sh` processes running. If so, cds to the `siteFolder`, adds changes, commits and pushes them. Remember to:

- __modify `source` in line 4 with the absolute path of `00_config.sh`__


