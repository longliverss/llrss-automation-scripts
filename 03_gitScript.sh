#!/usr/bin/bash

# Config file where variables are stored
source /home/username/AutomationFolder/00_config.sh

echo "__03 !!! $(date +%Y-%m-%d_%H%M%S) -- gitScript03 launched originally by the 02 script of $1" >> $logFile

#Create the Locking dir if it doesn't exist
if [[ ! -d $lockDir03 ]]; then
    mkdir -p $lockDir03 &>> $logFile
fi

#Check if there is currently a lock in place, if so then exit, if not then create a lock
if [ -f "${lockDir03}"*".lock" ]; then
    echo "__03 !!! $(date +%Y-%m-%d_%H%M%S) -- there's a lockfile03. $0 is currently already running."  >> $logFile
    echo "__03 !!! $(date +%Y-%m-%d_%H%M%S) -- Script $0 launched by $1: out. Exit"  >> $logFile
    exit
else
    touch $lockFile03 &>> $logFile
    echo "__03 !!! $(date +%Y-%m-%d_%H%M%S) -- lockfile03  $lockFile03 created" >> $logFile
fi

# Remover el lockfile02
if [ -f $lockFile02 ]; then
    rm "$lockFile02" &>> $logFile
    echo "__03 !!! $(date +%Y-%m-%d_%H%M%S) -- The 02lockfile for $1 is now released. Lets git it up" >> $logFile
else
    echo "__03 !!! $(date +%Y-%m-%d_%H%M%S) -- ERROR: No se borró el 02lockfile $1, pues no existía. (quién lo borró entonces?)" >> $logFile
fi

#git process
echo "__03 !!! $(date +%Y-%m-%d_%H%M%S) -- Git process started" &>> $logGitFile
cd $siteFolder &>> $logGitFile
git add . &>> $logGitFile
git commit -m "Automatic commit on $gitMessage" &>> $logGitFile
git push origin master &>> $logGitFile

#release the lock
rm "$lockFile03" &>> $logFile
echo "__03 !!! $(date +%Y-%m-%d_%H%M%S) -- The lock for $0 is now released and I am exiting." >> $logFile

