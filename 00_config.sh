#!/usr/bin/bash

# This file contains the variables used in subsequent scripts. Modify as needed.

#01

baseDir="/home/username/AutomationFolder/" # The folder in which scipts are stored and lockfiles created.
siteFolder="/home/username/project-site/" # path of jekyll project
targetDir="${siteFolder}img/" # Folder where imgs are being copied to.
filenamePrefix="llrss" # prefix used in every file processed

sleepTime01=10 # Wait time between image processing and 02_sleeplock launch.
chopIfHeight=1520 # If you want to chop certain images, set here default height
chopTop=140 # pixels to chop on top
chopBottom=143 # pixels to chop on bottom
minQualityPng=50 # png Compression min settings
maxnQualityPng=70 # png Compression max settings
maxnQualityjpg=70 # Jpg Compression settings

# unless you have to, there's no need to modify the variables from here on
lockDir01="${baseDir}lockdir_01/" # stores lockfiles for every image being copied

logFile="${baseDir}log.txt"
sleepScript02="${baseDir}02_sleeplock.sh"
gitScript03="${baseDir}03_gitScript.sh"

# 02
lockDir02="${baseDir}lockdir_02/"
lockFile02="${lockDir02}lockfile02.lock"
counter=0
sleepTime02=10 # Wait time checking whether image processing has ended
maxCounterTimes=500

#git
lockDir03="${baseDir}lockdir_03/"
lockFile03="${lockDir03}lockfile03.lock"
sleepTime03=30 # Wait time if a gitScript is in process before copying new files

logGitFile="${baseDir}logGit.txt" 
gitMessage=$(date +%Y-%m-%d_%H%M%S)