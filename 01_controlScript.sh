#!/usr/bin/bash

# Config file where variables are stored
source /home/username/AutomationFolder/00_config.sh

count=1
lockFile01="${lockDir01}${2}.lock"

echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- script $0 iniciado !" >> $logFile

# Check if there's a git commit in process
while [ -f "${lockDir03}"*".lock" ]
do
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- There's a git commit in process. wait $sleepTime03 seconds for the ${count}th time."  >> $logFile
    sleep $sleepTime03
    count=$((count + 1))
    if [ "$count" -gt $maxCounterTimes ]; then
        echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- I've been waiting too long for git to end: $count times. Copy process cancelled "  >> $logFile
        exit
    fi
done

# Crea el lockdir si no existe
if [[ ! -d $lockDir01 ]]; then
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- no hay lockdir. Procedemos a crearlo:" >> $logFile
    mkdir -p $lockDir01 &>> $logFile
fi

# Revisamos si hay un lockfile en uso. Si es así, exit; si no, creamos el lockfile
if [ -f $lockFile01 ]; then
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Hay un lockfile para el archivo $2, así que una instancia de $0 ya está siendo ejecutada. Exit."  >> $logFile
    exit
else
    touch $lockFile01 &>> $logFile
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- lockfile para $2 creado" >> $logFile
fi

# copying
echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Nuevo archivo $2 encontrado" >> $logFile

echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Copying $2" >> $logFile
cp $1 $targetDir &>> $logFile

# chopping
picHeight=`identify -format '%h' $1`
echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- $2 height is $picHeight" >> $logFile

if [[ $chopIfHeight == $picHeight ]]; then
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Chopping top $2" >> $logFile
    convert $targetDir$2 -strip -chop 0x$chopTop $targetDir$2 &>> $logFile
    
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Chopping bottom $2" >> $logFile
    convert $targetDir$2 -gravity South -chop 0x$chopBottom $targetDir$2 &>> $logFile
else
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Not chopping this one $2" >> $logFile
fi

# Compressing
if [[ $2 == *.png ]]; then
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Compressing with pngquant $2" >> $logFile
    pngquant --quality=${minQualityPng}-${maxnQualityPng} $targetDir$2 -strip --skip-if-larger --ext .png --force &>> $logFile
elif [[ $2 == *.jpg ]] || [[ $2 == *.jpeg ]] ; then
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Compressing with jpegoptim $2" >> $logFile
    jpegoptim $targetDir$2 --max=${maxnQualityjpg} -s &>> $logFile
fi

# Crea el directorio de la fecha en cuestion
dailyFolder="${targetDir}$(date +%Y%m%d)"
if [[ ! -d $dailyFolder ]]; then
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- no hay carpeta para la fecha de hoy. Procedemos a crearla:" >> $logFile
    mkdir -p $dailyFolder &>> $logFile
fi

# renaming
echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Renaming $2" >> $logFile
formerName=$targetDir$2
md5hash=$(md5sum $formerName)
extension="${2##*.}"
newName="${filenamePrefix}_$(date +%Y%m%d%H%M%S)_${md5hash:0:6}.${extension}"
mv "$formerName" "$dailyFolder/$newName" &>> $logFile

#waiting
echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Waiting $sleepTime01 seconds before launching 02 script" >> $logFile
sleep $sleepTime01

echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Iniciando $sleepScript02 con parámetro $lockFile01" >> $logFile
$sleepScript02 $lockFile01 &>> $logFile

# Remover el lockfile

if [ -f $lockFile01 ]; then
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- Hay un lockfile para el archivo $2. Vamo'a borra'lo."  >> $logFile
    rm "$lockFile01" &>> $logFile
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- The lock for $2 is now released and I am exiting." >> $logFile

else
    echo "__01 !!! $(date +%Y-%m-%d_%H%M%S) -- No se borró el lockfile de $2, pues ya estaba borrado" >> $logFile
fi
