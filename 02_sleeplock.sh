#!/usr/bin/bash

# Config file where variables are stored
source /home/username/AutomationFolder/00_config.sh

echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- Script 02 launched by $1" >> $logFile

#Create the Locking dir if it doesn't exist
if [[ ! -d $lockDir02 ]]; then
    mkdir -p $lockDir02 &>> $logFile
fi

#Check if there is currently a 02lock in place, if so then exit, if not then create a lock
if [ -f "${lockDir02}"*".lock" ]; then
    echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- there's a lockfile02, so $0 is currently already running."  >> $logFile
    echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- Script $0 launched by $1: out. Exit"  >> $logFile
    exit
else
    touch $lockFile02 &>> $logFile
    echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- lockfile02  $lockFile02 created" >> $logFile
fi

# Check for 01lockfiles to check if files are being processed. If so, wait. When there are no more lockfiles01, continue to launch gitscript03
while true
do
    #Remove the 01lockfile that lauched this script.
    if [ -f "$1" ]; then
        rm "$1" &>> $logFile
        echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- lockfile01 for $1 removed" >> $logFile
    fi

    # Check for 01lockfiles to check if files are being processed. 
    if [ -z "$(ls -A "$lockDir01")" ]; then
        echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) --  No more lockfiles. We can start this." >> $logFile
        break
    else
        counter=$((counter + 1))
        echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- There's already lockfiles for files being copied." >> $logFile
        if [ "$counter" -gt $maxCounterTimes ]; then
            echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- I've been waiting too long for files to finish copying: $counter times. Lockfile02 $lockFile02 will not be erased. Exit "  >> $logFile
            exit
        fi
        echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- Imma wait $sleepTime02 seconds for the $counter time to check if all files were copied." >> $logFile
        sleep $sleepTime02
    fi
done

#launch gitScript03
echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- lets git it. gitScript03 launched with parameter $1" >> $logFile
$gitScript03 $1 &>> $logFile

# Remover el lockfile02
if [ -f $lockFile02 ]; then
    echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- Hay un lockfile para el archivo $0. No debería"  >> $logFile
    rm "$lockFile02" &>> $logFile
    echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- The lock for $0 is now released and I am exiting." >> $logFile

else
    echo "__02 !!! $(date +%Y-%m-%d_%H%M%S) -- No se borró el lockfile $0, pues lo borró el gitScript03" >> $logFile
fi